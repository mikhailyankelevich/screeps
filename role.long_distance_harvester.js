var upgrader = require('role.upgrader');
module.exports = {
    // a function to run the logic for this role
    run: function(creep) {
        // console.log(creep.memory.working );
        // if creep is bringing energy to a structure but has no energy left
        if (creep.memory.working == true && creep.carry.energy == 0) {
            // switch state
            // console.log('yobanaya suka 1');
            creep.memory.working = false;

        }
        // if creep is harvesting energy but is full
        else if (creep.memory.working == false && creep.carry.energy == creep.carryCapacity) {
            // switch state
            creep.memory.working = true;
            // console.log('yobanaya suka 2');
        }

        // if creep is supposed to transfer energy to a structure
        if (creep.memory.working == true) {
            // if in home room
            // console.log('yobanaya suka 3');
            if (creep.room.name == creep.memory.home) {
                // console.log('yobanaya suka 4');
                // find closest spawn, extension or tower which is not full
                var structure = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
                    // the second argument for findClosestByPath is an object which takes
                    // a property called filter which can be a function
                    // we use the arrow operator to define it
                    filter: (s) => (s.structureType == STRUCTURE_SPAWN
                        || s.structureType == STRUCTURE_EXTENSION
                        || s.structureType == STRUCTURE_TOWER
                        || s.structureType == STRUCTURE_CONTAINER
                        || s.structureType == STRUCTURE_STORAGE)
                        && s.energy < s.energyCapacity
                });

                // if we found one
                if (structure != undefined) {
                    // console.log('yobanaya suka 5');
                    // try to transfer energy, if it is not in range
                    if (creep.transfer(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        // move towards it
                        creep.moveTo(structure);
                    }
                }else {
                    upgrader.run(creep);
                }
            }
            // if not in home room...
            else {
                // console.log('yobanaya suka 6');
                // find exit to home room
                var exit = creep.room.findExitTo(creep.memory.home);
                // and move to exit
                creep.moveTo(creep.pos.findClosestByRange(exit));
            }
        }
        // if creep is supposed to harvest energy from source
        else {
            // console.log('yobanaya suka 7');
            // if in target room
            if (creep.room.name == creep.memory.target) {
                // find source
                // console.log('yobanaya suka 8');
                var source = creep.room.find(FIND_SOURCES)[creep.memory.sourceIndex];

                // try to harvest energy, if the source is not in range
                if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                    // console.log('yobanaya suka 9');
                    // move towards the source
                    creep.moveTo(source);
                }
            }
            // if not in target room
            else {
                // console.log('yobanaya suka 10');
                // find exit to target room
                creep.say('where is an exit ?');
                var exit = creep.room.findExitTo(creep.memory.target);
                console.log(creep.memory.target);
                // move to exit
                creep.moveTo(creep.pos.findClosestByPath(exit));
            }
        }
    }
};