

var roleHarvester = require('role.harvester');
// private function to calculate the source priority
sourcePriority = function (source) {
    let priority;
    if (source.ticksToRegeneration == undefined) {
        priority = 10;
    } else if (source.energy == 0) {
        priority = 0;
    } else {
        priority = source.energy / source.ticksToRegeneration;
    }
    if (priority > 0 && source.ticksToRegeneration < 150) {
        priority = priority * (1 + (150 - source.ticksToRegeneration) / 250);
        if (source.ticksToRegeneration < 70) {
            priority = priority + (70 - source.ticksToRegeneration) / 10;
        }
    }
    return priority;
};


//modules to export
module.exports = {
    collectEnergyFrom: function (creep) {

        var resource = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES);
        var structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            // the second argument for findClosestByPath is an object which takes
            // a property called filter which can be a function
            // we use the arrow operator to define it
            filter: (s) => (s.structureType == STRUCTURE_CONTAINER)
                && s.store[RESOURCE_ENERGY] > 0
        });
        var reserve = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            // the second argument for findClosestByPath is an object which takes
            // a property called filter which can be a function
            // we use the arrow operator to define it
            filter: (s) => (s.structureType == STRUCTURE_CONTAINER|| s.structureType == STRUCTURE_SPAWN
                || s.structureType == STRUCTURE_EXTENSION
                || s.structureType == STRUCTURE_TOWER
                || s.structureType == STRUCTURE_STORAGE)
                && s.store[RESOURCE_ENERGY] > 0
        });
        if (creep.pickup(resource) == ERR_NOT_IN_RANGE&& creep.pos.getRangeTo(resource)< creep.pos.getRangeTo(structure)) {

            creep.moveTo(resource, {reusePath: 10});
        } else {

            // console.log('u ', structure);
            if (structure != undefined) {
                if (creep.withdraw(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {

                    creep.moveTo(structure, {reusePath: 3});
                } else if (creep.withdraw(reserve, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(reserve, {reusePath: 3});
                }
            }
            else {
                roleHarvester.run(creep);
            }
        }

    },

    findBestSource: function (creep) {
        let sources = creep.room.find(FIND_SOURCES);
        let used;


        if (creep.memory.preferedSource == null || creep.memory.preferedSource == undefined ) {
            let switchSource = _.random(0, 2) == 0;
            // let switchSource= 0;
            console.log('new source');
            if (sourcePriority(sources[1]) > sourcePriority(sources[0])) {
                if (switchSource==0) {
                    used = sources[0];
                } else {
                    used = sources[1];
                }
            } else {
                if (switchSource==0) {
                    used = sources[1];
                } else {
                    used = sources[0];
                }
            }
            // console.log('name : ',used.id,"    ",used);
            creep.memory.preferedSource = used.id;

        }else {
            for (var i of sources) {
                // console.log('name : ',i.id,"    ",i);
                // console.log(creep.memory.preferedSource);
                // console.log(creep.memory.preferedSource == i.id);
                if (creep.memory.preferedSource == i.id){

                    // this part makes miners go and find other source if they have gathered everything
                    // todo test out and see if useful


                    // if(i.energy == 0)
                    //     creep.memory.preferedSource=null;
                    used=i;
                    break;
                }
            }
        }
        return used;
    }
};