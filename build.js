module.exports = {
    run: function (NomSpawner) {
        var sources = Game.spawns[NomSpawner].room.find(FIND_SOURCES);
        // console.log(Game.spawns[NomSpawner].room.find(FIND_FLAGS));
        for (var a in Game.spawns[NomSpawner].room.find(FIND_FLAGS)){
            sources.push(Game.spawns[NomSpawner].room.find(FIND_FLAGS)[a]);
            // console.log(Game.spawns[NomSpawner].room.find(FIND_FLAGS)[a]);
        }
        // console.log(sources);
        //r
        for (var j = 1; j < sources.length; j++) {
            var chemin = Game.spawns[NomSpawner].pos.findPathTo(sources[j].pos);
            for (var i = 1; i < chemin.length; i++) {
                Game.spawns[NomSpawner].room.createConstructionSite(chemin[i].x, chemin[i].y, STRUCTURE_ROAD);
            }
        }
        // var flags = Game.spawns[NomSpawner].room.find(FIND_FLAGS);
        // console.log(flags);
        // for (var j = 1; j < flags.length; j++) {
        //     var chemin = Game.spawns[NomSpawner].pos.findPathTo(flags[j].pos);
        //     for (var i = 1; i < chemin.length; i++) {
        //         Game.spawns[NomSpawner].room.createConstructionSite(chemin[i].x, chemin[i].y, STRUCTURE_ROAD);
        //     }
        // }
    },
    runToWalls: function (NomSpawner) {
        var structures = Game.spawns[NomSpawner].room.find();
        console.log("src");
        for (var j = 1; j < structures.length; j++) {
            var chemin = Game.spawns[NomSpawner].pos.findPathTo(structures[j].pos);
            for (var i = 1; i < chemin.length; i++) {
                Game.spawns[NomSpawner].room.createConstructionSite(chemin[i].x, chemin[i].y, STRUCTURE_ROAD);
            }
        }
    }
};