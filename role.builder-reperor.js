/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.harvester');
 * mod.thing == 'a thing'; // true
 */
var roleBuilder = require('role.builder');
var actions = require('basicCreepsActions');
module.exports = {

    run: function (creep) {
        // console.log('harvester run');
        //change state of working if have no energy

        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        } else if (creep.memory.working == false && creep.carry.energy == creep.carryCapacity) {
            creep.memory.working = true;
        }

        //what to do if working and not . If not look for more energy
        if (creep.memory.working == true) {
            var structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                // TODO: check why
                filter: (c) => c.hits<c.hitsMax && c.structureType != STRUCTURE_WALL
            });
            // console.log(constructionSite)
            if (structure != undefined) {

                if (creep.repair(structure) == ERR_NOT_IN_RANGE) {
                    // if (creep.transfer(Game.spawns.Spawn1, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    // console.log('building');
                    creep.moveTo(structure);
                }
            } else{
                creep.say("acting like a builder");
                roleBuilder.run(creep);
                // console.log('not building');

            }
        } else {
            actions.collectEnergyFrom(creep);
            // // var source = creep.pos.findClosestByPath(FIND_SOURCES);
            // // // console.log(creep.harvest(source) );
            // // if (creep.harvest(source) == ERR_NOT_IN_RANGE ) {
            // //     // console.log('harvesting');
            // //     creep.moveTo(source)
            // // }
            // var structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            //     // the second argument for findClosestByPath is an object which takes
            //     // a property called filter which can be a function
            //     // we use the arrow operator to define it
            //     filter: (s) => (s.structureType == STRUCTURE_CONTAINER)
            //         && s.store[RESOURCE_ENERGY] > 0
            // });
            // if (structure != null)
            //     if (creep.withdraw(structure, RESOURCE_ENERGY)== ERR_NOT_IN_RANGE) {
            //         creep.moveTo(structure, {reusePath: 3});
            //     }
        }
    }
};