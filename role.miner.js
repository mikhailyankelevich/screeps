/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.harvester');
 * mod.thing == 'a thing'; // true
 */
var actions=require('basicCreepsActions');
module.exports = {

    run: function (creep) {
        // console.log('harvester run');
        //change state of working if have no energy

        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        } else if (creep.memory.working == false && creep.carry.energy == creep.carryCapacity) {
            creep.memory.working = true;
        }

        //what to do if working and not . If not look for more energy
        if (creep.memory.working == true) {
            // creep.say("w miner");
            var structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                // the second argument for findClosestByPath is an object which takes
                // a property called filter which can be a function
                // we use the arrow operator to define it
                filter: (s) => (s.structureType == STRUCTURE_CONTAINER)
                    && s.store[RESOURCE_ENERGY] < s.store.getCapacity()
            });
            // console.log(structure, "   ");

            // if (structure!=undefined){
            //     creep.moveTo(structure);
            // }
            if (creep.transfer(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.say('mtc');
                creep.moveTo(structure);

            }
        } else {
            // creep.say("-w miner");
            // var source = creep.pos.findClosestByPath(FIND_SOURCES);

            var source = actions.findBestSource(creep);
            // console.log(creep.name,' wants a source ', source);
            if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                creep.moveTo(source, {reusePath: 3});
                // creep.say("Send nudes");
            }
        }
    }
};