/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.updater');
 * mod.thing == 'a thing'; // true
 */
var actions = require('basicCreepsActions');

module.exports = {

    run: function (creep) {
        // console.log('upgrader run');
        //change state of working if have no energy

        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        } else if (creep.memory.working == false && creep.carry.energy == creep.carryCapacity) {
            creep.memory.working = true;
        }

        //what to do if working and not . If not look for more energy
        if (creep.memory.working == true) {
            if (creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                // if (creep.transfer(Game.spawns.Spawn1, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller);
                creep.say('w c');
            }
        } else {
            // var source = creep.pos.findClosestByPath(FIND_SOURCES);
            // if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
            //     creep.moveTo(source,{ignoreCreeps: true, reusePath: 1})
            // }
            var resource = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES);
            if (creep.pickup(resource) == ERR_NOT_IN_RANGE) {
                creep.moveTo(resource, {reusePath: 3});
            } else {
                actions.collectEnergyFrom(creep);
                // var structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                //     // the second argument for findClosestByPath is an object which takes
                //     // a property called filter which can be a function
                //     // we use the arrow operator to define it
                //     filter: (s) => (s.structureType == STRUCTURE_CONTAINER)
                //         && s.store[RESOURCE_ENERGY] > 0
                // });
                // // console.log('u ', structure);
                // if (structure != null)
                //     if (creep.withdraw(structure, RESOURCE_ENERGY)== ERR_NOT_IN_RANGE) {
                //         creep.moveTo(structure, {reusePath: 3});
                //     }

            }
        }
    }
};
