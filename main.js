// hello
// Game.spawns["Spawn1"].createCreep([WORK, CARRY, MOVE, MOVE])
// Game.creeps[creep].memory.role = 'upgrader'
require('prototype.spawn')();
var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleBuilderReparor = require('role.builder-reperor');
var roleBuilderWall = require('role.builderWall');
var roleRepairorWall = require('role.builderWallRepair');
var build = require('build');
var roleLongDistanceHarvester = require("role.long_distance_harvester");
var roleClaimer = require("role.clamer");
var roleMiner = require('role.miner');
var roleDeliverer = require('role.deliverer');
const HOME = 'E23S55';
const statusPrint = true;
// var r=require("nodemailer");
module.exports.loop = function () {


    // clear memory
    for (let name in Memory.creeps)
        if (Game.creeps[name] == undefined)
            delete Memory.creeps[name];


    // console.log("Energy on spawns is: " + Game.spawns.energy);
    for (let name in Game.creeps) {
        //initializing a creep to use

        var creep = Game.creeps[name];
        // creep.memory.working = true;
        if (creep.memory.role == "upgrader")
            roleUpgrader.run(creep);
        else if (creep.memory.role == "builder")
            roleBuilder.run(creep);
        else if (creep.memory.role == "repair")
            roleBuilderReparor.run(creep);
        else if (creep.memory.role == "harvester")
            // creep.memory.role = 'deliverer';
            roleHarvester.run(creep);
        else if (creep.memory.role == "wall_builder") {
            roleBuilderWall.run(creep);
        } else if (creep.memory.role == "longDistanceHarvester") {
            // creep.suicide();
            // console.log('harv');
            // creep.memory.role ='builder';
            // creep.memory.home = HOME;
            // creep.memory.target = 'W23N22';
            // creep.memory.sourceIndex = 0;
            // creep.memory.working = false;
            // // creep.memory.working = true;
            roleLongDistanceHarvester.run(creep);
            // creep.memory.role = 'miner';
        } else if (creep.memory.role == "claimer") {
            roleClaimer.run(creep);
        } else if (creep.memory.role == "wallRepairor") {
            roleRepairorWall.run(creep);
        } else if (creep.memory.role == 'miner'){
            // creep.memory.role = 'builder';
            roleMiner.run(creep);
        }else if (creep.memory.role == 'deliverer'){
            roleDeliverer.run(creep);
        }
        else {
            console.log('else');
            // creep.memory.role = 'longDistanceHarvester';
        }

    }


    var towers = _.filter(Game.structures, s => s.structureType == STRUCTURE_TOWER);
    for (let tower of towers) {
        var target = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if (target != undefined) {
            tower.attack(target);
        }
    }


    // name = Game.spawns.Spawn1.createCreep([ATTACK, CARRY, MOVE], undefined,
    //        {role: 'upgrader', working: false});

    // separate as it is a default and minimum value doesn't apply to it
    var builderCount = _.sum(Game.creeps, (c) => c.memory.role == 'builder');
    var builderMin = 9;

    // creating new creeps
    const totalMax = 23;
    var totalCount = _.sum(Game.creeps, (c) => true);

    var harvestersMin = 1;
    var harvestersCount = _.sum(Game.creeps, (c) => c.memory.role == 'harvester');

    var upgradersMin = 3;
    var upgradersCount = _.sum(Game.creeps, (c) => c.memory.role == 'upgrader');

    var reparorMin = 3;
    var reparorCount = _.sum(Game.creeps, (c) => c.memory.role == 'repair');

    var builderWallCount = _.sum(Game.creeps, (c) => c.memory.role == 'wall_builder');
    var builderWallMin = 0;

    var longDistHarvisterCount = _.sum(Game.creeps, (c) => c.memory.role == 'longDistanceHarvester');
    var longDistHarvisterMin = 2;

    var repairorWallMin = 1;
    var repairorWallCount = _.sum(Game.creeps, (c) => c.memory.role == 'wallRepairor');

    // can be ONLY used after lvl 3
    var claimerMax = 0;
    var claimerCount = _.sum(Game.creeps, (c) => c.memory.role == 'claimer');

    var minerMin =2;
    var minerMax=5;
    var minerCount = _.sum(Game.creeps, (c) => c.memory.role == 'miner');

    var delivererMin = 2;
    var delivererCount = _.sum(Game.creeps, (c) => c.memory.role == 'deliverer');

    //printing a current status of the colony
    if (statusPrint)
        console.log('t h m del u b r w wr ldh creeps: ',
            totalCount,
            harvestersCount,
            minerCount,
            delivererCount,
            upgradersCount,
            builderCount,
            reparorCount,
            builderWallCount,
            repairorWallCount,
            longDistHarvisterCount);

    // if(minerCount<2 && delivererCount<1)
    //     harvestersCount+=1;
    var energy = Game.spawns.Spawn1.room.energyCapacityAvailable *0.75;
    var name = undefined;
    if (totalCount < totalMax)
    // TODO: use in a day!
        if (claimerMax > claimerCount) {
            name = Game.spawns.Spawn1.createClaimer('E23S56');
            console.log('claimer made');

        } else if (harvestersCount < harvestersMin) {
            // try to spawn one
            name = Game.spawns.Spawn1.createCustomCreep(energy, 'harvester');
            console.log('harv');
            // if spawning failed and we have no harvesters left
            if (name == ERR_NOT_ENOUGH_ENERGY && harvestersCount < 2) {
                // spawn one with what is available
                name = Game.spawns.Spawn1.createCustomCreep(
                    Game.spawns.Spawn1.room.energyAvailable, 'harvester');
            }
        }else if (minerCount < minerMin) {
            console.log('miner');
            // name = Game.spawns.Spawn1.createCreep([WORK, CARRY, MOVE, MOVE], undefined,
            //     {role: 'repair', working: false});
            name = Game.spawns.Spawn1.createMiner(750,1 );
        } else if (delivererCount < delivererMin) {
            console.log('deliverer');
            // name = Game.spawns.Spawn1.createCreep([WORK, CARRY, MOVE, MOVE], undefined,
            //     {role: 'repair', working: false});
            name = Game.spawns.Spawn1.createDeliverer(950);
        } else if (upgradersCount < upgradersMin) {
            console.log('upgrader');
            // name = Game.spawns.Spawn1.createCreep([WORK, WORK, CARRY, MOVE], undefined,
            //     {role: 'upgrader', working: false});
            name = Game.spawns.Spawn1.createUpgrader(energy);
        }  else if (minerCount < minerMax) {
            console.log('miner');
            // name = Game.spawns.Spawn1.createCreep([WORK, CARRY, MOVE, MOVE], undefined,
            //     {role: 'repair', working: false});
            name = Game.spawns.Spawn1.createMiner(750,1 );
        }else if (longDistHarvisterCount < longDistHarvisterMin) {
            console.log('long dist');
            name = Game.spawns.Spawn1.createLongDistanceHarvester(energy, 1, HOME, 'E23S56', 0);
        } else if (reparorCount < reparorMin) {
            console.log('repairor');
            // name = Game.spawns.Spawn1.createCreep([WORK, CARRY, MOVE, MOVE], undefined,
            //     {role: 'repair', working: false});
            name = Game.spawns.Spawn1.createCustomCreep(energy, 'repair');
        } else if (repairorWallCount < repairorWallMin) {
            console.log('repairor wall');
            name = Game.spawns.Spawn1.createCustomCreep(energy, 'wallRepairor');
        } else if (builderWallCount < builderWallMin) {
            console.log('wall builder');
            // name = Game.spawns.Spawn1.createCreep([WORK, CARRY, MOVE, MOVE], undefined,
            //     {role: 'repair', working: false});
            name = Game.spawns.Spawn1.createCustomCreep(energy, 'wall_builder');
        }else if (builderCount<builderMin) {
            console.log('builder');
            // name = Game.spawns.Spawn1.createCreep([WORK, WORK, CARRY, MOVE], undefined,
            //     {role: 'builder', working: false});
            name = Game.spawns.Spawn1.createCustomCreep(energy, 'builder');
        }else{
            console.log('upgrader default');
            // name = Game.spawns.Spawn1.createCreep([WORK, WORK, CARRY, MOVE], undefined,
            //     {role: 'upgrader', working: false});
            name = Game.spawns.Spawn1.createUpgrader(energy);
        }
    // console.log(name);
    if (!(name < 0)) {
        console.log('New creep created', +name)
    }

    // Game.spawns.Spawn1.createClaimer('E17N9');
    // build.run("Spawn1");
    // build.runToWalls("Spawn1");
    //     Game.spawns.Spawn1.room.controller.activateSafeMode();


    var lastResort = require("room.failSafe");
    lastResort.saveMyRoom(HOME);
};

global.buildRoads = function (name) {
    build.run(name);
};
// Memory
//In game utils
global.killAll = function () {
    _.forEach(Game.creeps, function (c) {
        c.suicide();
    })
};
// pls work